@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="jumbotron">
            <h1 id="h1" style="display: none">Congratulations!</h1>
            <a href="#" id="money" class="btn btn-default" value="{{ Auth::user()->money }}">money
                {{ Auth::user()->money }}
            </a>
            <a href="#" id="points" class="btn btn-default" value="{{ Auth::user()->points }}">points
                {{ Auth::user()->points }}
            </a>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">Призы</div>
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div id="gift" class="panel-body"></div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div id="btn" class="btn btn-lg btn-success">Get your gift</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="success-btn" class="btn btn-lg btn-success" style="display: none">Беру</div>
            <div id="warning-btn" class="btn btn-lg btn-warning" style="display: none">Не нравится</div>

        </div>

        <div class="body-content">
        </div>
    </div>
@endsection
