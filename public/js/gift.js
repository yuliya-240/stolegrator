const cancel = () => {
    $('#h1').css('display', 'none');
    $('#success-btn').css('display', 'none');
    $('#warning-btn').css('display', 'none');
    $('#btn').css('display', 'inline-block');
    $('#gift').text("");
}
function getGift(){
    $.get('/gift', result =>{
        $('#h1').css('display', 'block');
        $('#btn').css('display', 'none');
        $('#success-btn')
            .css('display', 'inline-block')
            .data('type', result.type)
            .data('gift', result.gift);
        $('#warning-btn')
            .css('display', 'inline-block')
            .click(() => cancel());
        let type = '';
        $('#gift').text("Ваш подарок: "+result.gift +" "+ result.type);
    }, 'json')
}

function saveGift(){
    let _this =$('#success-btn')
    $.get('/gift/save',
        {
            type: _this.data('type'),
            gift: _this.data('gift'),
            status: 0
        },
            result =>{
        console.log(result);
    }, 'json')
}

$(document).ready(() => {
    $('#btn').click(() => getGift())
    $('#success-btn').click(() => saveGift())
})