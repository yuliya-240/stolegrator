<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class GiftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $type = rand(0, 2);

        switch ($type) {
            case 0:
                return ['type' => 'money', 'gift' => rand(100, 10000)];

            case 1:
                return ['type' => 'points', 'gift' => rand(100, 1000)];

            case 2:
                $gifts = [
                    "something 1", "something 2", "something 3"
                ];
                $i = array_rand($gifts);
                return ['type' => 'object', 'gift' => $gifts[$i]];
        }

    }

    public function save()
    {
        $users = DB::table('users')->get();
        $money = DB::table('users')->value('money');

        var_dump($money);
        if($_GET['type']=='money'){
            DB::table('users')
                ->where('id', 1)
                ->update(['money' => $_GET['gift']]);
        }
    }
}
